use rocket_contrib::json::Json;

#[derive(serde::Serialize)]
pub struct Error {
    error: u16,
    description: &'static str
}

pub static ERROR404: Error = Error {
    error: 404,
    description: "Not found"
};

#[catch(404)]
pub fn catch404(_req: &rocket::Request) -> Json<&'static Error> {
    Json(&ERROR404)
}
