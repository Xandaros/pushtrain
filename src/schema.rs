table! {
    application (id) {
        id -> Varchar,
        secret -> Varchar,
    }
}

table! {
    message (id) {
        id -> Integer,
        service -> Varchar,
        title -> Varchar,
        content -> Text,
    }
}

table! {
    permission (id) {
        id -> Integer,
        service -> Varchar,
        user -> Integer,
        perm -> Varchar,
    }
}

table! {
    service (id) {
        id -> Varchar,
        secret -> Varchar,
        parent -> Nullable<Varchar>,
        owner -> Integer,
        name -> Varchar,
        description -> Text,
    }
}

table! {
    subscription (id) {
        id -> Integer,
        application -> Varchar,
        service -> Varchar,
    }
}

table! {
    user (id) {
        id -> Integer,
        name -> Varchar,
        password -> Binary,
        admin -> Bool,
    }
}

joinable!(message -> service (service));
joinable!(permission -> service (service));
joinable!(permission -> user (user));
joinable!(service -> user (owner));
joinable!(subscription -> application (application));
joinable!(subscription -> service (service));

allow_tables_to_appear_in_same_query!(
    application,
    message,
    permission,
    service,
    subscription,
    user,
);
