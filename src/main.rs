#![feature(proc_macro_hygiene, decl_macro)]

extern crate base64;
#[macro_use] extern crate diesel;
extern crate rand;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
extern crate uuid;

pub mod errors;
pub mod models;
pub mod schema;
mod service;

#[get("/")]
fn index() -> &'static str {
    "Nothing to see here, move along"
}

fn main() {
    use crate::service::*;
    use crate::errors::*;
    rocket::ignite()
        .attach(models::DbConn::fairing())
        .mount("/", routes![index, list_services, get_service, new_service, delete_service])
        .register(catchers![catch404])
        .launch();
}
