use rocket::http::Status;
use diesel::prelude::*;
use rocket_contrib::json::Json;
use rand::Rng;

use super::models;
use super::models::DbConn;
use super::schema;

#[derive(serde::Deserialize)]
pub struct NewService {
    name: String
}

#[get("/service")]
pub fn list_services(db: DbConn) -> String {
    use schema::service::dsl::*;
    let result = service.load::<models::Service>(&db.0).expect("Error");
    serde_json::to_string_pretty(&result).expect("Error")
}

#[get("/service/<req_id>")]
pub fn get_service(db: DbConn, req_id: String) -> Result<String, Status> {
    use schema::service::dsl::*;
    let result = match service.filter(id.eq(req_id)).limit(1).first::<models::Service>(&db.0) {
        Ok(res) => res,
        Err(_) => return Err(Status::NotFound)
    };
    Ok(serde_json::to_string_pretty(&result).expect("Error"))
}

#[post("/service", data="<body>")]
pub fn new_service(db: DbConn, body: Json<NewService>) -> Json<models::Service> {
    use schema::service::dsl::*;

    let new_id = uuid::Uuid::new_v4().to_string();
    let mut secret_bytes = [0u8; 16];
    rand::thread_rng().fill(&mut secret_bytes);
    let new_secret = base64::encode(&secret_bytes);

    let new_service = models::Service {
        name: body.name.clone(),
        id: new_id,
        secret: new_secret,
        description: String::from(""),
        owner: 0,  // TODO: Needs actual owner
        parent: None
    };

    diesel::insert_into(service).values(&new_service).execute(&db.0).expect("Error");
    Json(new_service)
}

#[delete("/service/<service_id>")]
pub fn delete_service(db: DbConn, service_id: String) {
    use schema::service::dsl::*;
    diesel::delete(service.filter(id.eq(service_id))).execute(&db.0).expect("Error");
}
