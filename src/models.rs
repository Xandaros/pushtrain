use super::schema::service;
use serde::{Serialize, Deserialize};

#[database("pushtrain")]
pub struct DbConn(diesel::MysqlConnection);

#[derive(Queryable, Insertable)]
#[derive(Serialize, Deserialize)]
#[derive(PartialEq)]
#[table_name = "service"]
pub struct Service {
    pub id: String,
    pub secret: String,
    pub parent: Option<String>,
    pub owner: i32,
    pub name: String,
    pub description: String
}
