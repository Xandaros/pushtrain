-- Your SQL goes here
CREATE TABLE `user` (
	id INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	password BINARY(40) NOT NULL,
	admin BOOL NOT NULL DEFAULT 0
) ENGINE = InnoDB;

CREATE TABLE service (
	id VARCHAR(36) PRIMARY KEY,
	secret VARCHAR(24) NOT NULL,
	parent VARCHAR(36) NULL,
	`owner` INT NOT NULL,
	name VARCHAR(64) NOT NULL,
	description TEXT NOT NULL DEFAULT "",
	INDEX idx_parent (parent),
	INDEX idx_owner (owner),
	CONSTRAINT service_parent_fk FOREIGN KEY (parent) REFERENCES service (id),
	CONSTRAINT service_owner_fk FOREIGN KEY (owner) REFERENCES user (id)
) ENGINE = InnoDB;

CREATE TABLE application (
	id VARCHAR(36) PRIMARY KEY,
	secret VARCHAR(24) NOT NULL
) ENGINE = InnoDB;

CREATE TABLE subscription (
	id INT PRIMARY KEY AUTO_INCREMENT,
	application VARCHAR(36) NOT NULL,
	service VARCHAR(36) NOT NULL,
	INDEX idx_application (application),
	INDEX idx_service (service),
	CONSTRAINT subscription_application_fk FOREIGN KEY (application) REFERENCES application (id),
	CONSTRAINT subscription_service_fk FOREIGN KEY (service) REFERENCES service (id)
) ENGINE = InnoDB;

CREATE TABLE message (
	id INT PRIMARY KEY AUTO_INCREMENT,
	service VARCHAR(36) NOT NULL,
	title VARCHAR(64) NOT NULL,
	content TEXT NOT NULL,
	INDEX idx_service (service),
	CONSTRAINT message_service_fk FOREIGN KEY (service) REFERENCES service (id)
) ENGINE = InnoDB;

CREATE TABLE permission (
	id INT PRIMARY KEY AUTO_INCREMENT,
	service VARCHAR(36) NOT NULL,
	`user` INT NOT NULL,
	perm VARCHAR(32) NOT NULL,
	INDEX idx_service (service),
	INDEX idx_user (`user`),
	CONSTRAINT permission_service_fk FOREIGN KEY (service) REFERENCES service (id),
	CONSTRAINT permission_user_fk FOREIGN KEY (user) REFERENCES user (id)
) ENGINE = InnoDB;
