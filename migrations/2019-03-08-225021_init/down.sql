-- This file should undo anything in `up.sql`
DROP TABLE permission;
DROP TABLE message;
DROP TABLE subscription;
DROP TABLE service;
DROP TABLE application;
DROP TABLE `user`;
